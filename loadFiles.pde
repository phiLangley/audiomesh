void getModelList(){
    
  File folder = new File(modelPath);    
  File[] listOfFiles = folder.listFiles();

  for (File file : listOfFiles) {
    if (file.isFile()) {
      modelList.append(file.getName());
    }
  }
  
  modelList.sort();
  println(modelList);
}


void getAudioList(){
    
  File folder = new File(audioPath);    
  File[] listOfFiles = folder.listFiles();

  for (File file : listOfFiles) {
    if (file.isFile()) {
      audioList.append(file.getName());
    }
  }
  
  audioList.sort();
  
}
