class meshMod{
  
  int index;
  String fileName;
  String[] lines;
  FloatList vList;
  
  float minZ = 1000000;
  float maxZ = -1000000;

  
  meshMod(int ind, String fp) {
    
    vList = new FloatList();
    
    index = ind;
    fileName=fp;
    getGeometry();
    sortGeometry();
    
  }
  
  
 void displayBaseMesh(){

    mesh.fill(255,0,255);
    mesh.noStroke();
 
    mesh.beginShape(TRIANGLES);
    for (int i=0; i<vList.size(); i+=9) {
      mesh.vertex(vList.get(i+0), vList.get(i+1), vList.get(i+2)); 
      mesh.vertex(vList.get(i+3), vList.get(i+4), vList.get(i+5));      
      mesh.vertex(vList.get(i+6), vList.get(i+7), vList.get(i+8));
    }
    mesh.endShape(CLOSE);
       
  }

  
  void displayPerlinMesh(){
   
    mesh.noFill();
    mesh.strokeWeight(1/SCALE);
    mesh.stroke(150,100);
 
    float var0, var1, var2, var3, var4, var5, var6, var7, var8;
    float amp = 0.5;
    float freq = 0.001;
    
    mesh.beginShape(TRIANGLES);
    for (int i=0; i<vList.size(); i+=9){
      freq = random(0.001,0.005);
      var0 = amp * noise(freq*millis(), vList.get(i+0));
      var1 = amp * noise(freq*millis(), vList.get(i+1));
      var2 = amp * noise(freq*millis(), vList.get(i+2));
      var3 = amp * noise(freq*millis(), vList.get(i+3));
      var4 = amp * noise(freq*millis(), vList.get(i+4));
      var5 = amp * noise(freq*millis(), vList.get(i+5));
      var6 = amp * noise(freq*millis(), vList.get(i+6));
      var7 = amp * noise(freq*millis(), vList.get(i+7));
      var8 = amp * noise(freq*millis(), vList.get(i+8));
      mesh.vertex(vList.get(i+0)+var0, vList.get(i+1)+var1, vList.get(i+2)+var2); 
      mesh.vertex(vList.get(i+3)+var3, vList.get(i+4)+var4, vList.get(i+5)+var5);      
      mesh.vertex(vList.get(i+6)+var6, vList.get(i+7)+var7, vList.get(i+8)+var8);
    }
    mesh.endShape(CLOSE);
    
    //println("mesh");
    
  }
  
  void displayPerlinMeshBlob(float _bassampX, float _bassampY, float _bassampZ){
   
    mesh.fill(200);
    //mesh.noStroke();
    //mesh.noFill();
    mesh.strokeWeight(1/SCALE);
    mesh.stroke(150,50);
 
    float var0, var1, var2, var3, var4, var5, var6, var7, var8;
    float ampX = map(_bassampX, 0, 200, 0.1, 0.5);
    float ampY = map(_bassampY, 0, 200, 0.1, 0.5);
    float ampZ = map(_bassampZ, 0, 200, 0.1, 0.5);
    float freq = 0.001;
    
    mesh.beginShape(TRIANGLES);
    for (int i=0; i<vList.size(); i+=9){
      //freq = random(0.001,0.005);
      var0 = ampX * noise(freq*millis(), vList.get(i+0));
      var1 = ampY * noise(freq*millis(), vList.get(i+1));
      var2 = ampZ * noise(freq*millis(), vList.get(i+2));
      var3 = ampX * noise(freq*millis(), vList.get(i+3));
      var4 = ampY * noise(freq*millis(), vList.get(i+4));
      var5 = ampZ * noise(freq*millis(), vList.get(i+5));
      var6 = ampX * noise(freq*millis(), vList.get(i+6));
      var7 = ampY * noise(freq*millis(), vList.get(i+7));
      var8 = ampZ * noise(freq*millis(), vList.get(i+8));
      mesh.vertex(vList.get(i+0)+var0, vList.get(i+1)+var1, vList.get(i+2)+var2); 
      mesh.vertex(vList.get(i+3)+var3, vList.get(i+4)+var4, vList.get(i+5)+var5);      
      mesh.vertex(vList.get(i+6)+var6, vList.get(i+7)+var7, vList.get(i+8)+var8);
    }
    mesh.endShape(CLOSE);
        
  }
  
  void sortGeometry(){
    
    int lineCount=0;
    String tempVert0[];
    String tempVert1[];
    String tempVert2[];
    
    while (lineCount<lines.length-3){
      
      lines[lineCount] = trim(lines[lineCount]);
      tempVert0 = split(lines[lineCount], " ");  
      
      if (tempVert0[0].equals("vertex")) {      
        
        tempVert1 = split(lines[lineCount+1], " ");
        tempVert2 = split(lines[lineCount+2], " ");  
           
        vList.append(float(tempVert0[1]));
        vList.append(float(tempVert0[2]));
        vList.append(float(tempVert0[3]));
        vList.append(float(tempVert1[1]));
        vList.append(float(tempVert1[2]));
        vList.append(float(tempVert1[3]));
        vList.append(float(tempVert2[1]));
        vList.append(float(tempVert2[2]));
        vList.append(float(tempVert2[3]));
        
        lineCount+=3;
        
      } else {
        lineCount++;
      }
    }
    
   ///get min/ max z value
   for (int i = 1; i<vList.size(); i+=3){
     if (vList.get(i)<minZ){ minZ = vList.get(i);}
     if (vList.get(i)>maxZ){ maxZ = vList.get(i);}
   }
   
   //println(minZ + " - " + maxZ);
   
   ///move model in z to midpoint
   for (int i = 1; i<vList.size(); i+=3){
     //println(vList.get(i));
     float tPt =  vList.get(i);
     vList.set(i, tPt - ((maxZ - minZ)/2));
   }
  
  }

  void getGeometry(){
    lines = loadStrings(modelPath + fileName);   
  }
  
}

void initializeMeshMod(){
  
  for (int i=0; i<modelList.size(); i++){
    meshModList.add(new meshMod(i, modelList.get(i)));
  }

}
