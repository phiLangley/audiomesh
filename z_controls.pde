////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////control the rotation and scale display of the model using the mouse
void mouseDragged() {
   
   if (mouseButton == LEFT) {
     ROTX -= (mouseX - pmouseX) * 0.01;
     ROTY -= (mouseY - pmouseY) * 0.01;
   }
   if (mouseButton == RIGHT) {
     if (mouseY>pmouseY){
        SCALE = SCALE+(mouseY - pmouseY);
     } else {
        SCALE = SCALE+(mouseY - pmouseY);
     }
     SCALE = abs(SCALE);
   }
   redraw();
}

void keyReleased(){

  
}

//////////////////audio controls

void keyPressed(){
  if(key == '+'){
    thresholds[stepBand]++;
  }
  else if(key == '-'){
    thresholds[stepBand]--;
  }
  if(key == 'v'){
    th++;
    th = min(th, numberBands-1);
  }
  else if(key == 'b'){
    th--;
    th = max(th,0);
  }
  if(key == 'n'){
    thresholds[th]++;
  }
  else if(key == 'm'){
    thresholds[th]--;
  }
  else if(key == 'a'){
    stepBand++;
    stepBand = min(stepBand, numberBands);
  }
  else if(key == 's'){
    stepBand--;
    stepBand = max(stepBand,0);
  }
  else if(key == 'w'){
    checkNextStepMs += 10;
  }
  else if(key == 'e'){
    checkNextStepMs -= 10;
  }
  else if(key == 'x'){
    bbackground = !bbackground;
  }
  else if(key == 'd'){
    highFreqBand++;
    highFreqBand = min(highFreqBand, numberBands);
  }
  else if(key == 'f'){
    highFreqBand--;
    highFreqBand = max(highFreqBand,0);
  }
}
