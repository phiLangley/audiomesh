// - audio
import ddf.minim.analysis.*;
import ddf.minim.*;
// -

String modelPath;
String audioPath;

ArrayList<meshMod> meshModList;
StringList modelList;
StringList audioList;

PGraphics mesh;

boolean distort = false;

float ROTX = PI;                        
float ROTY = PI;
float SCALE = 80;

// - audio

Minim minim;  
AudioInput in;
FFT dataFFT;

float spectrumScale = 100;
int sampleRate = 1024;
int numberBands = 32;
int stepBand = numberBands/2;
int highFreqBand = numberBands-1;
int[] thresholds = new int[numberBands];
int th = stepBand;

boolean bstep = false;
boolean bcheckstep = true;
int checkNextStepMs = 330;
int justCheckedStepMs;

int totalSteps = 0;
// - 

float bassampX, bassampY, bassampZ;

boolean bbackground = true;
int backgroundCounter = 0;

void setup(){
  
  size(800,800, P3D);
  
  meshModList = new ArrayList();
  modelList = new StringList();
  audioList = new StringList();
  
  modelPath = savePath("") + "/data/models/";
  getModelList();
  println(modelList);
  initializeMeshMod();

  mesh = createGraphics(width, height, P3D);
  
  //audioPath = savePath("") + "/data/audio/";
  //getAudioList();
  //println(audioList);
  
  // - audio
  minim = new Minim(this);
  // use the getLineIn method of the Minim object to get an AudioInput
  in = minim.getLineIn();
  
  dataFFT = new FFT( in.bufferSize(), sampleRate );
  
  dataFFT.linAverages(numberBands);
  
  // thresholds init
  for(int i=0; i<numberBands; i++){
    thresholds[i] = (int)random(10, 40);
  }
  thresholds[highFreqBand] = 10;
  // - 
  
  
}

void draw(){
  
  ///draw mesh////////////////////////////////////////////////////
  mesh.beginDraw();
  
	if(bbackground){
		if (distort==true){
		  mesh.background(255);
		} else {
		  mesh.background(0);
		}
	}
      mesh.pushMatrix();
    
        mesh.translate(width/2, height/2);
        
        mesh.rotateY(ROTX);
        mesh.rotateX(ROTY);
        mesh.scale(SCALE);
        
        mesh.directionalLight(255, 255, 255, 0, -1, 0);
        //mesh.ambientLight(255, 255, 255);
              
        if (distort==true){
          meshModList.get(0).displayPerlinMesh();
          distort=false;
        } else {
          meshModList.get(0).displayPerlinMeshBlob(bassampX, bassampY, bassampZ);
        }

    mesh.popMatrix();
  mesh.endDraw();
  
  image(mesh, 0, 0);
  ///////////////////////////////////////////////////////
  
  
  ///draw audio/////////////////////////////////////////
  rectMode(CORNERS);
  
  // - audio
  if(bcheckstep == false){
    if(millis() - justCheckedStepMs > checkNextStepMs){
      bcheckstep = true;
    }
  }
  
  // perform a forward FFT on the samples
  dataFFT.forward(in.mix);
  
  bassampZ = dataFFT.getAvg(0)*spectrumScale;
  bassampY = dataFFT.getAvg(1)*spectrumScale;
  bassampX = dataFFT.getAvg(2)*spectrumScale;
  
  // draw the full spectrum
  noFill();
  stroke(255,100);
  for(int i=0; i<dataFFT.specSize(); i++){
    line(i, height, i, height - dataFFT.getBand(i)*spectrumScale);
  }
  
  // draw the linear averages
  int w = int(width/dataFFT.avgSize());
  stroke(255);
  for(int i = 0; i < dataFFT.avgSize(); i++){
    // draw a rectangle for each average, multiply the value by spectrumScale so we can see it better
    if(stepBand == i){
      fill(255,200);
      bstep = false;
      if( (bcheckstep==true) && (dataFFT.getAvg(i)*spectrumScale > thresholds[stepBand]) ){
        bstep = true;
        distort = true;
        totalSteps++;
        justCheckedStepMs = millis();
        bcheckstep = false;
      }
    }
    else{
      fill(255,100);
    }   
    rect(i*w, height, i*w + w, height - dataFFT.getAvg(i)*spectrumScale);
    
    /*
    if(highFreqBand == i){
      if((backgroundCounter<=0) && (dataFFT.getAvg(i)*spectrumScale > thresholds[highFreqBand])){
        bbackground = false;
        backgroundCounter = 5;
      }
      if(backgroundCounter>=0){
        backgroundCounter--;
      }
      else{
        bbackground = true;
      }
    }
    */
  }
  stroke(255,0,60);
  strokeWeight(5);
  line(0,height-thresholds[stepBand], width, height-thresholds[stepBand]);
  stroke(255,100,160);
  strokeWeight(1);
  for(int i=0; i<numberBands; i++){
    line(i*w,height-thresholds[i], i*w+w, height-thresholds[i]);
  }
  
  
  float alfa = map(millis()-justCheckedStepMs, 0,checkNextStepMs, 255, 0);
  fill(255, alfa);
  noStroke();
  ellipse(10,10, 20,20);
  
  audioDrawDebugInfo(w);
  // -
    
}


void audioDrawDebugInfo(int _w){
  fill(255,200);
  for(int i = 0; i < dataFFT.avgSize(); i++){
    text(i, 2+i*_w, height - 10);
  }
  text("thresholds[stepBand] [++|--]: " + thresholds[stepBand], 20, 20);
  text("step band [a+|s-]: " + stepBand, 20, 40);
  text("checkNextStepMs [w+|e-]: " + checkNextStepMs, 20, 60);
  text("totalSteps: " + totalSteps, 20, 80);
  text("th [v+|b-]: " + th, 20, 120);
  text("thresholds[th] [n+|m-]: " + thresholds[th], 20, 140);
  text("high freq band [d+|f-]: " + highFreqBand, 20, 160);
}
